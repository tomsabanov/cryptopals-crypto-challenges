/*
Challenge 3 of set 1
 */

#include <stdio.h>
#include <ctype.h>
#include <float.h>
#include <stdlib.h>

static inline int hexVal(char c)
{
    if (c >= 'a' && c <= 'f')
        return 10 + c - 'a';
    if (c >= 'A' && c <= 'F')
        return 10 + c - 'A';
    if (c >= '0' && c <= '9')
        return c - '0';
    return 0;
}


char* xorCipher(char* text, int key, int len){
    char* xor = (char*)malloc(sizeof(char) * len);
    char* tmp = xor;
    int i = 0;
    char* t = text;
    while(i<len){
        char c = (hexVal(t[0]) << 4) | hexVal(t[1]);
        *tmp = c ^ (char)key;
        t+=2;
        tmp++;
        i+=2;
    }
    return xor;
}

double eval(char *text, int len){
    double  FREQ[] = { 
                0.0651738, 0.0124248, 0.0217339, 0.0349835,  //'A', 'B', 'C', 'D',...
                0.1041442, 0.0197881, 0.0158610, 0.0492888, 
                0.0558094, 0.0009033, 0.0050529, 0.0331490,
                0.0202124, 0.0564513, 0.0596302, 0.0137645, 
                0.0008606, 0.0497563, 0.0515760, 0.0729357, 
                0.0225134, 0.0082903, 0.0171272, 0.0013692, 
                0.0145984, 0.0007836, 0.1918181, 0.1
            };
    char* ALPH = "abcdefghijklmnopqrstuvwxyz";

    int length = sizeof(FREQ)/sizeof(FREQ[0]);
    int observed[length];
    for(int i = 0; i<length; i++){
        observed[i] = 0;
    }


    for(int i = 0; i<len; i++){
        if((text[i] >= 'a' && text[i] <= 'z') || (text[i] >= 'A' && text[i] <= 'Z')){
              int c = tolower(text[i]) - 'a';
              observed[c] = observed[c] + 1;
        }
        else if(text[i] == ' ' || text[i] == '\n'){
            observed[length-2] += 1;
        }
        else{
            observed[length-1] -= 1;
        }
    }

    double sum = 0;
    for(int i = 0; i<length; i++){
        double sub = observed[i] - FREQ[i]*(len-1);
        double tmp = (sub * sub)/(len * FREQ[i]);
        sum += tmp;
    }
    return sum;
}

double cipher(char* text, int len){
    double min = DBL_MAX;
    int key = -1;
    for(int i = 0; i<= 255; i++){
        char* tmp = xorCipher(text, i, len);
        
        double sc = eval(tmp,len);
        if(sc>=0 && sc < min){    
           // printf("KEY %d, score %f,  %s\n",key,min, tmp);
            min = sc;
            key = i;
        }
    }

    char* final = xorCipher(text, key, len);
    printf("BEST KEY %d, score %f,  %s\n",key,min, final);
    return min;
}
double cipherBest(char* text, int len, char* final){
    double min = DBL_MAX;
    int key = -1;
    for(int i = 0; i<= 255; i++){
        char* tmp = xorCipher(text, i, len);
        
        double sc = eval(tmp,len);
        if(sc>=0 && sc < min){    
           // printf("KEY %d, score %f,  %s\n",key,min, tmp);
            min = sc;
            key = i;
        }
    }

    final = xorCipher(text, key, len);
    printf("BEST KEY %d, score %f,  %s\n",key,min, final);
    return min;
}

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

static inline int hexVal(char c)
{
    if (c >= 'a' && c <= 'f')
        return 10 + c - 'a';
    if (c >= 'A' && c <= 'F')
        return 10 + c - 'A';
    if (c >= '0' && c <= '9')
        return c - '0';
    return 0;
}

char* fixedXor(char* a, char* b){
    // xor HEX values a and b into  hex c
    char* HEX = "0123456789abcdef";

    if(strlen(a) != strlen(b)){
        return NULL;
    }
    char* c = (char*)malloc(sizeof(char) * strlen(a));

    char* tmp = c;
    while(*a){
        int xor = hexVal(*a) ^ hexVal(*b);
        *tmp = HEX[xor%16];
        a++;
        b++;
        tmp++;
    }
    return c;
}

int main(){
    char* a = "1c0111001f010100061a024b53535009181c";
    char* b = "686974207468652062756c6c277320657965";

    char* c = fixedXor(a, b);
    printf("%s\n", c);
}
#include <stdio.h>
#include <ctype.h>
#include "hexToBase64.h"

char* xorText(char* a, char* b){
    // xor values a and b into  hex c
    char* HEX = "0123456789abcdef";
    if(strlen(a) != strlen(b)){
        return NULL;
    }
    char* c = (char*)malloc(sizeof(char) * strlen(a)*2);

    char* tmp = c;
    while(*a){
        int xor = *a ^ *b;
        *tmp = HEX[(xor & 0xF0) >> 4];
        tmp++;
        *tmp = HEX[xor & 0x0F];
        a++;
        b++;
        tmp++;
    }
    return c;
}


char* repeatXor(char* text, char* xor){
    char* repeat = (char*)malloc(sizeof(char) * strlen(text));
    int xorLen = strlen(xor);

    char* tmp = repeat;
    for(int i = 0; i<strlen(text); i++){
        *tmp = xor[i%xorLen];
        tmp++;
    }

    char* xored = xorText(text, repeat);
    return xored;
}


int main(){
    char* text = "Burning 'em, if you ain't quick and nimble";
    char* text2 = "I go crazy when I hear a cymbal";

    char* encrypted = repeatXor(text, "ICE");
    printf(" Encrypted %s\n", encrypted);
}
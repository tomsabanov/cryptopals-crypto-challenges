#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include "xorCipher.h"

char** toBlock(char* file, int keysize){
    int x, numRead;
    FILE *fp = fopen(file,"r");
    fseek(fp, 0, SEEK_END);
    int fileSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    printf("FileSize %d \n", fileSize);

    int rem = 0;
    if(fileSize%keysize != 0) rem = 1;
    int blockSize = fileSize/keysize + rem;
    char** block = (char**)calloc(sizeof(char*), blockSize);

    char** tmp = block;
    for(x = 0; x < fileSize; x += keysize) {
        *tmp = (char*)calloc(sizeof(char) , keysize);
        char* tmp_string = *tmp;
        for(int i = 0; i<keysize; i++){
            char c = fgetc(fp);
            tmp_string[i] = c;
        }
        //printf("%s ", *tmp);
        tmp++;
    }
    
    // block moramo transponirati
    char** transposed = (char**)calloc(sizeof(char*),keysize);
    char** tmp_transposed = transposed;
    char** tmp_block = block;
    for(int j = 0; j<keysize; j++){
        *tmp_transposed = (char*)calloc(sizeof(char), blockSize);
        char* tmp = *tmp_transposed;
        for(int i = 0; i<blockSize; i++){
                *tmp = (*tmp_block)[j];
               // *tmp_transposed[i] = *tmp_block[j];
                tmp_block++;
                tmp++;
        }    
        tmp_transposed++;  
        tmp_block = block;
    }
    

    fclose(fp);
    return transposed;
}

int byteDistance(char a, char b){
        char z  = a ^ b;
        int r = 0;
        for (; z > 0; z >>= 1) {
            r += (z & 0x01) ? 1 : 0;
        }
        return r;
}
int getHammingDistance(char* a, char* b, int len){
    char* tmp_a = a;
    char* tmp_b = b;
    int distance = 0;

    for(int i = 0; i<len; i++){
        distance += byteDistance(*tmp_a, *tmp_b);
        tmp_a++;
        tmp_b++;
    }
    return distance;
}

double calcHammingDistance(char* file, int KEYSIZE, int blocks){
    FILE * fp;
    fp = fopen(file, "r");
    if (fp == NULL) exit(EXIT_FAILURE);

    fseek(fp, 0L, SEEK_END);
    size_t filesize = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    double dist = 0.0;
    char* a = calloc(sizeof(char), KEYSIZE * blocks);
    char* b = calloc(sizeof(char), KEYSIZE * blocks);
    fgets(a, KEYSIZE * blocks, fp);
    fgets(b,KEYSIZE * blocks, fp);

    dist += (double)getHammingDistance(a,b, KEYSIZE*blocks);
    fclose(fp);
    return dist;
}

void decrypt(char* file){
    int KEYSIZE = 2;
    int key = 2;
    double min = DBL_MAX;

    for(; KEYSIZE <= 40; KEYSIZE++){
        int blocks = 1;
        double dist = (double)calcHammingDistance(file,KEYSIZE, blocks);

        printf("Keysize %d, distance %f\n", KEYSIZE, dist);
        if(dist < min){
            min = dist;
            key = KEYSIZE;
        }
    }

    //char** block = toBlock(file, key);
 

    //printf("Keysize : %d\n", key);
    //printf("Min is %f\n", min);
}




int main(){
   decrypt("6.txt");
   printf("%d\n", getHammingDistance("B","M",2));
   printf("%d\n", getHammingDistance("abd","acd",3));
}
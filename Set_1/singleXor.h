#include "xorCipher.h"
#include <stdio.h>
#include <stdlib.h>

int main(){
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./singleXor.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    double min = DBL_MAX;
    char* best = NULL;
    while ((read = getline(&line, &len, fp)) != -1) {
        char* enc = line;
        double score = cipher(enc, 60);
        if(score < min){
            min = score;
            best = (char*)malloc(sizeof(char) * strlen(line));
            strcpy(best, enc);
        }
    }

    char* orig = malloc(sizeof(char) * strlen(best));
    cipherBest(best, 60, orig);


    fclose(fp);
    if (line)
        free(line);
    exit(EXIT_SUCCESS);

}